package com.unmsm.gps.app.danielneciosup.nikki.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 02/10/2016.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter
{
    private Context context;

    private final List<Fragment> mFragments = new ArrayList<>();
    private final int[] iconsTab = NikkiUtils.ARRAY_ICONS_TAB_LAYOUT;

    public SectionsPagerAdapter(FragmentManager fm, Context context)
    {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem( int position )
    {
        return mFragments.get( position );
    }

    @Override
    public int getCount()
    {
        return mFragments.size();
    }

    public void addFragment( Fragment fragment )
    {
        mFragments.add( fragment );
    }

    public CharSequence getPageTitle( int position )
    {
        Drawable iconTab = context.getResources().getDrawable( iconsTab[ position ] );
        iconTab.setBounds( 0, 0, iconTab.getIntrinsicWidth(), iconTab.getIntrinsicHeight() );
        SpannableString spannableString = new SpannableString( " " );
        ImageSpan imageSpan = new ImageSpan( iconTab, ImageSpan.ALIGN_BOTTOM );
        spannableString.setSpan( imageSpan, 0, 1, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE );

        return spannableString;
    }
}
