package com.unmsm.gps.app.danielneciosup.nikki.bean;

/**
 * Created by Daniel on 16/10/2016.
 */
public class Category
{
    private int idCategory;
    private String txtNombre;
    //private int idCategoryFather;


    public Category() {
    }

    public Category(int idCategory, String txtNombre)
    {
        this.idCategory = idCategory;
        this.txtNombre = txtNombre;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(String txtNombre) {
        this.txtNombre = txtNombre;
    }

    /*public int getIdCategoryFather() {
        return idCategoryFather;
    }

    public void setIdCategoryFather(int idCategoryFather) {
        this.idCategoryFather = idCategoryFather;
    }*/
}
