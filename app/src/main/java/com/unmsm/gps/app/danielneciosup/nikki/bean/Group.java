package com.unmsm.gps.app.danielneciosup.nikki.bean;

import java.sql.Date;

/**
 * Created by Daniel on 16/10/2016.
 */
public class Group
{
    private int idGroup;
    private String txtDescrip;
    private Date dtCreate;

    public Group(int idGroup, String txtDescrip, Date dtCreate) {
        this.idGroup = idGroup;
        this.txtDescrip = txtDescrip;
        this.dtCreate = dtCreate;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getTxtDescrip() {
        return txtDescrip;
    }

    public void setTxtDescrip(String txtDescrip) {
        this.txtDescrip = txtDescrip;
    }

    public Date getDtCreate() {
        return dtCreate;
    }

    public void setDtCreate(Date dtCreate) {
        this.dtCreate = dtCreate;
    }
}
