package com.unmsm.gps.app.danielneciosup.nikki.bean;

import java.util.Date;

/**
 * Created by Daniel on 16/10/2016.
 */
public class Note
{
    private int noteId;
    private String txDescrip;
    private Date dtCreate;
    private Date dtRun;
    private String nomGroup;
    private int numAlarms;
    private int imgviewNote;
    private Category category;
    private Group group;
    private User user;

    public Note() {
    }

    public Note(int noteId, String txDescrip, Date dtCreate, Date dtRun, String nomGroup, int numAlarms)
    {
        this.noteId = noteId;
        this.txDescrip = txDescrip;
        this.dtCreate = dtCreate;
        this.dtRun = dtRun;
        this.nomGroup = nomGroup;
        this.numAlarms = numAlarms;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getNoteId()
    {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getTxDescrip() {
        return txDescrip;
    }

    public void setTxDescrip(String txDescrip) {
        this.txDescrip = txDescrip;
    }

    public Date getDtCreate() {
        return dtCreate;
    }

    public void setDtCreate(Date dtCreate) {
        this.dtCreate = dtCreate;
    }

    public Date getDtRun() {
        return dtRun;
    }

    public void setDtRun(Date dtRun) {
        this.dtRun = dtRun;
    }

    public String getNomGroup() {
        return nomGroup;
    }

    public void setNomGroup(String nomGroup) {
        this.nomGroup = nomGroup;
    }

    public int getNumAlarms() {
        return numAlarms;
    }

    public void setNumAlarms(int numAlarms) {
        this.numAlarms = numAlarms;
    }

    public int getImgviewNote() {
        return imgviewNote;
    }

    public void setImgviewNote(int imgviewNote) {
        this.imgviewNote = imgviewNote;
    }
}
