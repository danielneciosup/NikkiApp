package com.unmsm.gps.app.danielneciosup.nikki.bean;

/**
 * Created by Daniel on 16/10/2016.
 */
public class User
{
    private int idUser;
    private String txtEmail;
    private String accessToken;

    public User() {
    }

    public User(int idUser, String txtEmail, String accessToken) {
        this.idUser = idUser;
        this.txtEmail = txtEmail;
        this.accessToken = accessToken;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getTxtEmail() {
        return txtEmail;
    }

    public void setTxtEmail(String txtEmail) {
        this.txtEmail = txtEmail;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
