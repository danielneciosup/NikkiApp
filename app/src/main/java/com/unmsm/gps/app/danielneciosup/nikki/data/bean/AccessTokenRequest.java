package com.unmsm.gps.app.danielneciosup.nikki.data.bean;

import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public class AccessTokenRequest
{
    private String email;
    private String password;

    public AccessTokenRequest(String email)
    {
        this.email = email;
        this.password = NikkiUtils.PASSWORD_UNIQUE;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
