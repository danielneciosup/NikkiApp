package com.unmsm.gps.app.danielneciosup.nikki.data.repository;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.CategoriesDataSource;

/**
 * Created by DINJO-PC on 23/11/2016.
 */
public class CategoryRepository implements CategoriesDataSource {

    private static CategoryRepository INSTANCE = null;

    private final CategoriesDataSource mCategoriesRemoteDataSource;
    private final CategoriesDataSource mCategoriesLocalDataSource;

    private CategoryRepository(CategoriesDataSource mCategoriesRemoteDataSource, CategoriesDataSource mCategoriesLocalDataSource) {
        this.mCategoriesRemoteDataSource = mCategoriesRemoteDataSource;
        this.mCategoriesLocalDataSource = mCategoriesLocalDataSource;
    }

    public static CategoryRepository getInstance(CategoriesDataSource mCategoriesRemoteDataSource, CategoriesDataSource mCategoriesLocalDataSource) {
        if( INSTANCE == null ){
            INSTANCE = new CategoryRepository(mCategoriesRemoteDataSource,mCategoriesLocalDataSource);
        }
        return INSTANCE;
    }

    @Override
    public void getCategories(@NonNull LoadCategoriesCallback callback) {
        mCategoriesLocalDataSource.getCategories(callback);
    }

    @Override
    public void getCategory(Integer categoryId, @NonNull GetCategoryCallback callback) {

    }

    @Override
    public void saveCategory(@NonNull Category category) {

    }

    @Override
    public void deleteCategory(@NonNull Integer categoryId) {

    }

    @Override
    public void insertCategory(@NonNull InsertCategoryCallback callback, Category category) {

    }

    @Override
    public void getCategoryByName(@NonNull GetCategoryCallback callback, String categoryName) {
        mCategoriesLocalDataSource.getCategoryByName(callback,categoryName);
    }

    @Override
    public void getCategoryByNoteTitle(@NonNull GetCategoryCallback callback, String noteTitle) {
        mCategoriesLocalDataSource.getCategoryByNoteTitle(callback,noteTitle);
    }

    @Override
    public void deleteAllCategories() {

    }
}
