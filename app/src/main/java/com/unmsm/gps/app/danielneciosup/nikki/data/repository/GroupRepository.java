package com.unmsm.gps.app.danielneciosup.nikki.data.repository;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.GroupsDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.GroupLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.GroupRemoteDataSource;

/**
 * Created by DINJO-PC on 19/11/2016.
 */
public class GroupRepository implements GroupsDataSource
{
    private static GroupRepository instance = null;
    private final GroupLocalDataSource  groupLocalDataSource;
    private final GroupRemoteDataSource groupRemoteDataSource;

    /**
     *
     * @param groupLocalDataSource source local of groups.
     * @param groupRemoteDataSource source remote of groups.
     */
    private GroupRepository(@NonNull GroupLocalDataSource groupLocalDataSource,
                           @NonNull GroupRemoteDataSource groupRemoteDataSource)
    {
        this.groupLocalDataSource = groupLocalDataSource;
        this.groupRemoteDataSource = groupRemoteDataSource;
    }

    public static GroupRepository getInstance(@NonNull GroupLocalDataSource groupLocalDataSource,
                                              @NonNull GroupRemoteDataSource groupRemoteDataSource)
    {
        if ( instance == null )
        {
            instance = new GroupRepository( groupLocalDataSource, groupRemoteDataSource );
        }

        return instance;
    }

    @Override
    public void getGroups(@NonNull LoadedGroupsCallback callback)
    {

    }

    @Override
    public void createGroup(@NonNull LoadedGroupCreateCallback callback, @NonNull GroupCreateRequest request)
    {
        groupRemoteDataSource.createGroup( callback, request );
    }

    @Override
    public void getUserAddedToGroup(@NonNull LoadedUserAddedCallback callback, @NonNull UserAddedToGroupRequest request)
    {
        groupRemoteDataSource.getUserAddedToGroup( callback, request );
    }
}
