package com.unmsm.gps.app.danielneciosup.nikki.data.repository;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.bean.Alarm;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.NotesDataSource;

/**
 * Created by Daniel on 30/10/2016.
 */
public class NotesRepository implements NotesDataSource
{
    private static NotesRepository INSTANCE = null;

    private final NotesDataSource mNotesRemoteDataSource;
    private final NotesDataSource mNotesLocalDataSource;

    //private NotesRepository(@NonNull NotesDataSource mNotesRemoteDataSource, @NonNull NotesDataSource mNotesLocalDataSource)
    private NotesRepository( NotesDataSource mNotesRemoteDataSource, NotesDataSource mNotesLocalDataSource)
    {
        this.mNotesRemoteDataSource = mNotesRemoteDataSource;
        this.mNotesLocalDataSource = mNotesLocalDataSource;
    }

    public static NotesRepository getInstance( NotesDataSource notesRemoteDataSource, NotesDataSource notesLocalDataSource )
    {
        if( INSTANCE == null )
            INSTANCE = new NotesRepository( notesRemoteDataSource, notesLocalDataSource );

        return INSTANCE;
    }

    @Override
    public void getNotesByUserId(@NonNull LoadNotesCallback callback, Integer userId) {

    }

    @Override
    public void getNote(@NonNull int noteId, @NonNull GetNoteCallback callback) {

    }

    @Override
    public void addAlarm(@NonNull int noteId, @NonNull Alarm alarm) {

    }

    @Override
    public void createNote(@NonNull InsertNoteCallback callback, @NonNull Note note) {
        mNotesLocalDataSource.createNote(callback,note);
    }

    @Override
    public void getNotesByCategoryId(@NonNull int categoryId) {

    }
}
