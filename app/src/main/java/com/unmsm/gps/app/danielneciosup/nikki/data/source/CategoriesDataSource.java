package com.unmsm.gps.app.danielneciosup.nikki.data.source;

import android.support.annotation.NonNull;

import java.util.List;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;


/**
 * Created by Daniel on 27/10/2016.
 */
public interface CategoriesDataSource
{
    interface LoadCategoriesCallback
    {
        void onCategoriesLoaded(List<Category> categories);
        void onDataNotAvailable();
    }

    interface GetCategoryCallback
    {
        void onCategoryLoaded( Category category );
        void onDataNotAvailable();
    }

    interface InsertCategoryCallback{
        void onCategoryInserted();
        void onCategoryNotInserted();
    }

    void getCategories( @NonNull LoadCategoriesCallback callback );
    void getCategory( Integer categoryId, @NonNull GetCategoryCallback callback );
    void saveCategory( @NonNull Category category );
    void deleteCategory( @NonNull Integer categoryId );
    void insertCategory( @NonNull InsertCategoryCallback callback, Category category );
    void getCategoryByName( @NonNull GetCategoryCallback callback, String categoryName );
    void getCategoryByNoteTitle( @NonNull GetCategoryCallback callback, String noteTitle );
    void deleteAllCategories();

}
