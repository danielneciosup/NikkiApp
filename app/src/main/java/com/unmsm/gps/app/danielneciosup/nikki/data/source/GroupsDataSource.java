package com.unmsm.gps.app.danielneciosup.nikki.data.source;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;

import java.util.List;

/**
 * Created by DINJO-PC on 19/11/2016.
 */
public interface GroupsDataSource
{
    interface LoadedGroupsCallback
    {
        void onGroupsLoaded(List<GroupResponse> groups);

        void onDataNotAvailable();

        void onErrorConnection();
    }

    interface LoadedGroupCreateCallback
    {
        void onGroupCreateLoaded(GroupCreateResponse groupCreateResponse);

        void onErrorService();

        void onErrorConnection();
    }

    interface LoadedUserAddedCallback
    {
        void onUserAddedLoaded(UserAddedToGroupResponse userAddedToGroupResponse);

        void onDataNotAvailable();

        void onUserIsAlreadAMember();

        void onErrorConnection();
    }

    void getGroups( @NonNull LoadedGroupsCallback callback );

    void createGroup( @NonNull LoadedGroupCreateCallback callback, @NonNull GroupCreateRequest request );

    void getUserAddedToGroup( @NonNull LoadedUserAddedCallback callback, @NonNull UserAddedToGroupRequest request );
}
