package com.unmsm.gps.app.danielneciosup.nikki.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.CategoriesDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite.NikkiDbContract;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite.NikkiDbHelper;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DINJO-PC on 23/11/2016.
 */
public class CategoryLocalDataSource implements CategoriesDataSource{

    private static CategoryLocalDataSource INSTANCE;

    private NikkiDbHelper mNikkiDbHelper;

    private CategoryLocalDataSource(@NonNull Context context)
    {
        mNikkiDbHelper = NikkiDbHelper.getInstance( context );
    }

    public static CategoryLocalDataSource getInstance( @NonNull Context context )
    {
        if ( INSTANCE == null )
            INSTANCE = new CategoryLocalDataSource( context.getApplicationContext() );

        return INSTANCE;
    }

    @Override
    public void getCategories(@NonNull LoadCategoriesCallback callback) {
        SQLiteDatabase db = mNikkiDbHelper.getReadableDatabase();
        String query = String.format("SELECT * FROM %s;",
                NikkiDbContract.CategoryEntry.TABLE_NAME);
        Cursor cursor = db.rawQuery(query,new String[]{ });
        if( cursor != null ){
            if( cursor.moveToFirst() ){
                List<Category> categories = new ArrayList<>();
                Category category;
                do{
                    category = new Category(
                        cursor.getInt(cursor.getColumnIndex(NikkiDbContract.CategoryEntry._ID)),
                        cursor.getString(cursor.getColumnIndex(NikkiDbContract.CategoryEntry.TX_NAME))
                    );
                    categories.add(category);
                }while(cursor.moveToNext());
                callback.onCategoriesLoaded(categories);
            }else{
                callback.onDataNotAvailable();
            }
        }else{
            callback.onDataNotAvailable();
        }

    }

    @Override
    public void getCategory(Integer categoryId, @NonNull GetCategoryCallback callback) {

    }

    @Override
    public void saveCategory(@NonNull Category category) {

    }

    @Override
    public void deleteCategory(@NonNull Integer categoryId) {

    }

    @Override
    public void insertCategory(@NonNull InsertCategoryCallback callback, Category category) {
        SQLiteDatabase db = mNikkiDbHelper.getWritableDatabase();
        ContentValues newCategory = new ContentValues();
        newCategory.put(NikkiDbContract.CategoryEntry.TX_NAME,category.getTxtNombre());

        long result =  db.insert(NikkiDbContract.CategoryEntry.TABLE_NAME, null, newCategory);

        if( result !=-1 ){
            callback.onCategoryInserted();
        }else{
            callback.onCategoryNotInserted();
        }
    }

    @Override
    public void getCategoryByName(@NonNull GetCategoryCallback callback, String categoryName) {
        SQLiteDatabase db = mNikkiDbHelper.getReadableDatabase();
        String query = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1",
                NikkiDbContract.CategoryEntry.TABLE_NAME,
                NikkiDbContract.CategoryEntry.TX_NAME);
        Cursor cursor = db.rawQuery(query,new String[]{ categoryName });
        if( cursor != null ){
            if( cursor.moveToFirst() ){
                Category category = new Category(
                        cursor.getInt(cursor.getColumnIndex(NikkiDbContract.CategoryEntry._ID)),
                        cursor.getString(cursor.getColumnIndex(NikkiDbContract.CategoryEntry.TX_NAME))
                        );
                callback.onCategoryLoaded(category);
            }else{
                callback.onDataNotAvailable();
            }
        }else{
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void getCategoryByNoteTitle(@NonNull GetCategoryCallback callback, String noteTitle) {
        SQLiteDatabase db = mNikkiDbHelper.getReadableDatabase();
        try {
            String query = String.format("SELECT * FROM %s WHERE LOWER(?) like LOWER('%%'||%s||'%%') " +
                    "OR %s = ? ORDER BY 1 DESC LIMIT 1;",
                    NikkiDbContract.CategoryEntry.TABLE_NAME,
                    NikkiDbContract.CategoryEntry.TX_NAME,
                    NikkiDbContract.CategoryEntry.TX_NAME);
            Cursor cursor = db.rawQuery(query, new String[]{noteTitle,NikkiUtils.DEFAULT_CATEGORY});
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    Category category = new Category(
                            cursor.getInt(cursor.getColumnIndex(NikkiDbContract.CategoryEntry._ID)),
                            cursor.getString(cursor.getColumnIndex(NikkiDbContract.CategoryEntry.TX_NAME))
                    );
                    callback.onCategoryLoaded(category);
                } else {
                    callback.onDataNotAvailable();
                }
            } else {
                callback.onDataNotAvailable();
            }
        }catch (Exception ex){
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void deleteAllCategories() {

    }
}
