package com.unmsm.gps.app.danielneciosup.nikki.data.source.local;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.GroupsDataSource;

/**
 * Created by DINJO-PC on 19/11/2016.
 */
public class GroupLocalDataSource implements GroupsDataSource
{
    private static GroupLocalDataSource instance;

    public static GroupLocalDataSource getInstance()
    {
        if ( instance == null )
        {
            instance = new GroupLocalDataSource();
        }

        return instance;
    }

    @Override
    public void getGroups(@NonNull LoadedGroupsCallback callback)
    {

    }

    @Override
    public void createGroup(@NonNull LoadedGroupCreateCallback callback, @NonNull GroupCreateRequest request)
    {

    }

    @Override
    public void getUserAddedToGroup(@NonNull LoadedUserAddedCallback callback, @NonNull UserAddedToGroupRequest request) {

    }
}
