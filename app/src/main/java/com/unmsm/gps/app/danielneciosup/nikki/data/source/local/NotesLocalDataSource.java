package com.unmsm.gps.app.danielneciosup.nikki.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.bean.Alarm;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Category;
import com.unmsm.gps.app.danielneciosup.nikki.bean.Note;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.CategoryRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.NotesDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite.NikkiDbContract;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite.NikkiDbHelper;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Daniel on 30/10/2016.
 */
public class NotesLocalDataSource implements NotesDataSource
{
    private static NotesLocalDataSource INSTANCE;

    private NikkiDbHelper mNikkiDbHelper;


    private NotesLocalDataSource(@NonNull Context context)
    {
        mNikkiDbHelper = NikkiDbHelper.getInstance( context );
    }

    public static NotesLocalDataSource getInstance( @NonNull Context context )
    {
        if ( INSTANCE == null )
            INSTANCE = new NotesLocalDataSource( context );

        return INSTANCE;
    }

    @Override
    public void getNotesByUserId( @NonNull LoadNotesCallback callback, Integer userId )
    {
        List<Note> notes = new ArrayList<>();
        SQLiteDatabase db = mNikkiDbHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery
        (
            "SELECT " +
            NikkiDbContract.NoteEntry.TABLE_NAME + "." + NikkiDbContract.NoteEntry._ID + "," +
            NikkiDbContract.NoteEntry.TABLE_NAME + "." + NikkiDbContract.NoteEntry.TX_DESCRIP + "," +
            NikkiDbContract.NoteEntry.TABLE_NAME + "." + NikkiDbContract.NoteEntry.DT_CREATE + "," +
            NikkiDbContract.NoteEntry.TABLE_NAME + "." + NikkiDbContract.NoteEntry.DT_RUN + "," +
            NikkiDbContract.GroupEntry.TABLE_NAME + "." + NikkiDbContract.GroupEntry.TX_NAME + "," +
            "COUNT( "+ NikkiDbContract.AlarmEntry.TABLE_NAME + ".* )" +
            "FROM " + NikkiDbContract.NoteEntry.TABLE_NAME +
                " LEFT  JOIN " + NikkiDbContract.AlarmEntry.TABLE_NAME + " ON " + NikkiDbContract.NoteEntry.TABLE_NAME + "." + NikkiDbContract.NoteEntry._ID + " = " + NikkiDbContract.AlarmEntry.TABLE_NAME + "." + NikkiDbContract.AlarmEntry.NOTE_ID +
                "       JOIN " + NikkiDbContract.UserEntry.TABLE_NAME  + " ON " + NikkiDbContract.NoteEntry.TABLE_NAME + "." + NikkiDbContract.NoteEntry.USER_ID + " = " + NikkiDbContract.UserEntry.TABLE_NAME + "." + NikkiDbContract.UserEntry._ID + " AND " + NikkiDbContract.UserEntry.TABLE_NAME + "." + NikkiDbContract.UserEntry._ID + " = ?" +
                " LEFT  JOIN " + NikkiDbContract.GroupEntry.TABLE_NAME + " ON " + NikkiDbContract.NoteEntry.TABLE_NAME + "." + NikkiDbContract.NoteEntry.GROUP_ID  + " = " + NikkiDbContract.GroupEntry.TABLE_NAME + "." + NikkiDbContract.GroupEntry._ID +
            " GROUP BY 1, 2, 3, 4, 5 " +
            " ORDER BY 1;", new String[]{ Integer.toString( userId ) }
        );

        if ( cursor != null && cursor.getCount() > 0 )
        {
            while ( cursor.moveToNext() )
            {
                int noteId = cursor.getInt( 1 );
                String txDescrip = cursor.getString( 2 ) ;
                Date dtCreate = NikkiUtils.stringToDate( cursor.getString( 3 ) ) ;
                Date dtRun = NikkiUtils.stringToDate( cursor.getString( 4 ) ) ;
                String txNameGroup = cursor.getString( 5 );
                int numAlarms = cursor.getInt( 6 );

                Note note = new Note( noteId, txDescrip, dtCreate, dtRun, txNameGroup, numAlarms );
                notes.add( note );
            }
        }

        if ( cursor != null )
            cursor.close();

        db.close();

        if ( notes.isEmpty() )
            callback.onDataNotAvailable();
        else
            callback.onTaskLoaded( notes );
    }

    @Override
    public void getNote(@NonNull int noteId, @NonNull GetNoteCallback callback)
    {

    }

    @Override
    public void addAlarm(@NonNull int noteId, @NonNull Alarm alarm)
    {

    }

    @Override
    public void createNote(@NonNull InsertNoteCallback callback, @NonNull Note note)
    {

        SQLiteDatabase db = mNikkiDbHelper.getWritableDatabase();

        try {
            ContentValues newNote = new ContentValues();
            newNote.put(NikkiDbContract.NoteEntry.TX_DESCRIP, note.getTxDescrip());
            newNote.put(NikkiDbContract.NoteEntry.DT_RUN, (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS")).format(note.getDtRun()));
            newNote.put(NikkiDbContract.NoteEntry.USER_ID, Integer.toString(note.getUser().getIdUser()));
            newNote.put(NikkiDbContract.NoteEntry.GROUP_ID, note.getCategory().getIdCategory());

            long result = db.insert(NikkiDbContract.NoteEntry.TABLE_NAME, null, newNote);

            if (result != -1) {
                callback.onNoteInserted();
            } else {
                callback.onNoteNotInserted();
            }
        }catch (Exception ex){
            callback.onNoteNotInserted();
        }
    }

    @Override
    public void getNotesByCategoryId(@NonNull int categoryId)
    {

    }
}
