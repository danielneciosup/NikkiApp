package com.unmsm.gps.app.danielneciosup.nikki.data.source.local.sqlite;

import android.provider.BaseColumns;

/**
 * Created by Daniel on 22/10/2016.
 */
public class NikkiDbContract
{
    private NikkiDbContract(){}

    public static abstract class CategoryEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "TA_CATEGORY";

        public static final String TX_NAME = "TX_NAME";

        public static final String COMMON_INSERT_COMMAND =
                String.format("INSERT INTO %s (%s) VALUES (?);",TABLE_NAME,TX_NAME);

        //public static final String CATEGORY_FATHER_ID = "CATEGORY_FATHER_ID";
    }

    public static abstract class GroupEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "TA_GROUP";

        public static final String TX_NAME = "TX_NAME";

        public static final String DT_CREATE = "DT_CREATE";
    }

    public static abstract class UserEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "TA_USER";

        public static final String TX_EMAIL = "TX_EMAIL";

        public static final String COMMON_INSERT_COMMAND =
                String.format("INSERT INTO %s (%s) VALUES (?);",TABLE_NAME,TX_EMAIL);

        //public static final String TX_ACCESS_TOKEN = "TX_ACCESS_TOKEN";
    }

    public static abstract class AlarmEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "TA_ALARM";

        public static final String HR_ALARM = "HR_ALARM";
        public static final String USER_ID = "ID_USER";
        public static final String NOTE_ID = "ID_NOTE";
    }

    public static abstract class NoteEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "TA_NOTE";

        public static final String TX_DESCRIP = "TX_DESCRIP";
        public static final String DT_CREATE = "DT_CREATE";
        public static final String DT_RUN = "DT_RUN";
        public static final String USER_ID = "ID_USER";
        public static final String CATEGORY_ID = "ID_CATEGORY";
        public static final String GROUP_ID = "ID_GROUP";
    }

    public static abstract class UserGroupEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "TA_USER_GROUP";

        public static final String USER_ID = "USER_ID";
        public static final String GROUP_ID = "GROUP_ID";
        public static final String BL_ISADMIN = "BL_ISADMIN";
    }
}
