package com.unmsm.gps.app.danielneciosup.nikki.data.source.remote;

import android.support.annotation.NonNull;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.GroupsDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service.GroupService;
import com.unmsm.gps.app.danielneciosup.nikki.util.RetrofitRemoteSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by DINJO-PC on 19/11/2016.
 */
public class GroupRemoteDataSource implements GroupsDataSource
{
    private static GroupRemoteDataSource instance;

    private static Retrofit retrofit;

    private static GroupService service;

    private GroupRemoteDataSource(){}

    public static GroupRemoteDataSource getInstance()
    {
        if ( instance == null )
        {
            instance = new GroupRemoteDataSource();
            retrofit = RetrofitRemoteSingleton.getRetrofit();
            service = retrofit.create( GroupService.class );
        }

        return instance;
    }

    @Override
    public void getGroups(@NonNull LoadedGroupsCallback callback)
    {

    }

    @Override
    public void createGroup(@NonNull final LoadedGroupCreateCallback callback, @NonNull GroupCreateRequest request)
    {
        Call<GroupCreateResponse> call = service.createGroup( request );
        call.enqueue(new Callback<GroupCreateResponse>()
        {
            @Override
            public void onResponse(Call<GroupCreateResponse> call, Response<GroupCreateResponse> response)
            {
                if ( response.isSuccessful() )
                {
                    callback.onGroupCreateLoaded(response.body());
                }
                else
                {
                    callback.onErrorService();
                }
            }

            @Override
            public void onFailure(Call<GroupCreateResponse> call, Throwable t)
            {
                callback.onErrorConnection();
            }
        });
    }

    @Override
    public void getUserAddedToGroup(@NonNull final LoadedUserAddedCallback callback, @NonNull UserAddedToGroupRequest request)
    {
        Call<UserAddedToGroupResponse> call = service.addUserToGroup( request );
        call.enqueue(new Callback<UserAddedToGroupResponse>()
        {
            @Override
            public void onResponse(Call<UserAddedToGroupResponse> call, Response<UserAddedToGroupResponse> response)
            {
                if ( response.isSuccessful())
                {
                    callback.onUserAddedLoaded( response.body() );
                }
                else if ( response.code() == 404 )
                {
                    callback.onDataNotAvailable();
                }
                else if ( response.code() == 400 )
                {
                    callback.onUserIsAlreadAMember();
                }
            }

            @Override
            public void onFailure(Call<UserAddedToGroupResponse> call, Throwable t)
            {
                callback.onErrorConnection();
            }
        });
    }
}
