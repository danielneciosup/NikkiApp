package com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.AccessTokenResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.AccessTokenRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public interface CreateUserService
{
   @POST( "users" )
   Call<AccessTokenResponse> sendCredentialsForCreateUser(@Body AccessTokenRequest request);
}
