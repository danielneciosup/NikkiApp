package com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public interface GroupService
{
    @POST( "users/create/group" )
    Call<GroupCreateResponse> createGroup(@Body GroupCreateRequest request);

    @POST( "users/added/users" )
    Call<UserAddedToGroupResponse> addUserToGroup(@Body UserAddedToGroupRequest request);
}
