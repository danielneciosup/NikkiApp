package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.adapter.MemberAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.bean.User;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public class AddMembersToGroupDFragment extends DialogFragment implements AddMembersToGroupContract.View
{
    private AddMembersToGroupContract.Presenter mPresenter;

    private static String nameGroup;

    @BindView( R.id.recview_members_group )
    RecyclerView recviewMembersGroup;

    @BindView( R.id.edittxt_email_member )
    EditText edittxtEmailMember;

    private MemberAdapter memberAdapter;

    public static AddMembersToGroupDFragment newInstance(GroupCreateResponse groupCreateResponse)
    {
        AddMembersToGroupDFragment fragment = new AddMembersToGroupDFragment();
        Bundle args = new Bundle();
        args.putString( "title", groupCreateResponse.getName() );
        fragment.setArguments( args );

        nameGroup = groupCreateResponse.getName();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate( R.layout.dfragment_add_member_to_group, container, false );
        ButterKnife.bind( this, mView );

        setPresenter( new AddMembersToGroupPresenter( this ) );

        setRecyclerView();

        setAdapter(new MemberAdapter.OnItemOptionDeleteClickListener()
        {
            @Override
            public void onItemDeleteOption(int position)
            {
                mPresenter.onDeleteMember( position );
            }
        });

        return mView;
    }

    public void setAdapter(MemberAdapter.OnItemOptionDeleteClickListener listener)
    {
        memberAdapter = new MemberAdapter(listener);
    }

    public void setRecyclerView()
    {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getContext() );
        ( ( LinearLayoutManager ) layoutManager).setOrientation( LinearLayoutManager.VERTICAL );
        recviewMembersGroup.setHasFixedSize( true );
        recviewMembersGroup.setLayoutManager( layoutManager );
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        String title = getArguments().getString( "title", nameGroup );
        getDialog().setTitle( title );
    }

    @Override
    public void displayMemberAdded(UserAddedToGroupResponse response)
    {
        MemberAdapter.add( response );
        onResumeMembers();
    }

    @Override
    public void showMembersAdded()
    {
        recviewMembersGroup.setAdapter( memberAdapter );
    }

    @Override
    public void setPresenter(AddMembersToGroupContract.Presenter presenter)
    {
        mPresenter = presenter;
    }

    @Override
    public void onResumeMembers()
    {
        super.onResume();
        mPresenter.onLoadAllUsersAdded();
    }

    @Override
    public void displayMemberDelete(int position)
    {
        MemberAdapter.delete(position);
        onResumeMembers();
    }

    @Override
    public void showMessageUserNotFound()
    {
        Toast.makeText( getContext(), "El usuario no tiene una cuenta en Nikki", Toast.LENGTH_LONG  ).show();
    }

    @Override
    public void showMessageMemberAlreadyExists()
    {
        Toast.makeText( getContext(), "El usuario ya ha sido agregado al grupo", Toast.LENGTH_LONG ).show();
    }

    @Override
    @OnClick( R.id.icon_add_member )
    public void displayAddMember()
    {
        mPresenter.onAddMember( new UserAddedToGroupRequest(edittxtEmailMember.getText().toString(), nameGroup ) );
    }

    @Override
    public void showMessageErrorConnection()
    {
        Toast.makeText( getContext(), "Error de conexión", Toast.LENGTH_LONG ).show();
    }
}
