package com.unmsm.gps.app.danielneciosup.nikki.group;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.UserAddedToGroupResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.repository.GroupRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.GroupsDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.GroupLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.GroupRemoteDataSource;

import java.util.List;

/**
 * Created by DINJO-PC on 20/11/2016.
 */
public class AddMembersToGroupPresenter implements AddMembersToGroupContract.Presenter
{
    private AddMembersToGroupContract.View mView;

    private GroupRepository groupRepository;

    public AddMembersToGroupPresenter(AddMembersToGroupContract.View mView)
    {
        this.mView = mView;
        groupRepository = GroupRepository.getInstance(GroupLocalDataSource.getInstance(),
                                                      GroupRemoteDataSource.getInstance());
    }

    @Override
    public void onAddMember(final UserAddedToGroupRequest request)
    {
        groupRepository.getUserAddedToGroup(new GroupsDataSource.LoadedUserAddedCallback()
        {
            @Override
            public void onUserAddedLoaded(UserAddedToGroupResponse userAddedToGroupResponse)
            {
                mView.displayMemberAdded( userAddedToGroupResponse );
            }

            @Override
            public void onDataNotAvailable()
            {
                mView.showMessageUserNotFound();
            }

            @Override
            public void onUserIsAlreadAMember()
            {
                mView.showMessageMemberAlreadyExists();
            }

            @Override
            public void onErrorConnection()
            {
                mView.showMessageErrorConnection();
            }
        }, request);
    }

    @Override
    public void onLoadAllUsersAdded()
    {
        mView.showMembersAdded();
    }

    @Override
    public void onDeleteMember(int position)
    {
        mView.displayMemberDelete(position);
    }
}
