package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public class ConfirmAddMembersDFragment extends DialogFragment implements ConfirmAddMembersContract.View
{
    private ConfirmAddMembersContract.Presenter mPresenter;

    private static String titleNameGroup;

    private static GroupCreateResponse createResponse;

    public static ConfirmAddMembersDFragment newInstance(GroupCreateResponse groupCreateResponse)
    {
        ConfirmAddMembersDFragment frag = new ConfirmAddMembersDFragment();
        Bundle args = new Bundle();
        args.putString( "title", groupCreateResponse.getName() );
        frag.setArguments( args );

        createResponse = groupCreateResponse;

        titleNameGroup = groupCreateResponse.getName();

        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate( R.layout.dfragment_confirm_add_members, container, false );
        ButterKnife.bind( this, mView );

        setPresenter( new ConfirmAddMembersPresenter( this ) );

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        String title = getArguments().getString( "title",  titleNameGroup);
        getDialog().setTitle( title );
    }

    @Override
    @OnClick( R.id.btn_confirm_add_members )
    public void displayConfirm()
    {
        FragmentManager fm = getFragmentManager();
        AddMembersToGroupDFragment addMembersToGroupDFragment = AddMembersToGroupDFragment.newInstance( createResponse );
        addMembersToGroupDFragment.show( fm, "fragment_add_members" );
        closeConfirmation();
    }

    @Override
    @OnClick( R.id.btn_cancel_confirm_add_members )
    public void displayDeny()
    {
        dismiss();
    }

    @Override
    public void setPresenter(ConfirmAddMembersContract.Presenter presenter)
    {
        mPresenter = presenter;
    }

    @Override
    public void closeConfirmation()
    {
        dismiss();
    }
}