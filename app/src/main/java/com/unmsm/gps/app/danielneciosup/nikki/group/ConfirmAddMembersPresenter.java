package com.unmsm.gps.app.danielneciosup.nikki.group;

import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;

/**
 * Created by DINJO-PC on 13/11/2016.
 */
public class ConfirmAddMembersPresenter implements ConfirmAddMembersContract.Presenter
{
    private ConfirmAddMembersContract.View mView;

    public ConfirmAddMembersPresenter(ConfirmAddMembersContract.View mView)
    {
        this.mView = mView;
    }

    @Override
    public void onConfirm(GroupCreateResponse groupCreateResponse)
    {
        onCloseConfirmation();
        mView.displayConfirm();
    }

    @Override
    public void onDeny()
    {
        mView.displayDeny();
    }

    @Override
    public void onCloseConfirmation()
    {
        mView.closeConfirmation();
    }
}
