package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public class CreateGroupDFragment extends DialogFragment implements CreateGroupContract.View
{
    private CreateGroupContract.Presenter mPresenter;

    private EditText editxtNameGroup;

    private Button btnCreateGroup;

    private Button btnCancelCreateGroup;

    public static CreateGroupDFragment newInstance(String title)
    {
        CreateGroupDFragment fragment = new CreateGroupDFragment();
        Bundle args = new Bundle();
        args.putString( "title", title );
        fragment.setArguments( args );

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mView = inflater.inflate(R.layout.dfragment_create_group, container, false );

        setPresenter( new CreateGroupPresenter( this, getContext() ) );

        return mView;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
    {
        super.onViewCreated(view, savedInstanceState);

        String title = getArguments().getString( "title", getString( R.string.txt_title_dfragment_create_group ) );
        getDialog().setTitle( title );

        initDialog( view );
    }

    private void initDialog( View view )
    {
        editxtNameGroup = (EditText) view.findViewById( R.id.editxt_name_group );
        btnCreateGroup = (Button) view.findViewById( R.id.btn_create_group );
        btnCancelCreateGroup = (Button) view.findViewById( R.id.btn_cancel_create_group );

        btnCreateGroup.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Toast.makeText( getContext(), "Crear Grupo", Toast.LENGTH_SHORT ).show();
                mPresenter.onCreateGroup( editxtNameGroup.getText().toString() );
            }
        });

        btnCancelCreateGroup.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                displayCancelCreateGroup();
            }
        });
    }

    @Override
    public void displayCreateSuccess(GroupCreateResponse groupCreateResponse)
    {
        Toast.makeText( getContext(), "Grupo creado con éxito", Toast.LENGTH_SHORT ).show();
    }

    @Override
    public void displayCancelCreateGroup()
    {
        dismiss();
    }

    @Override
    public void displayCreateError()
    {
        Toast.makeText( getContext(), "Este grupo ya existe", Toast.LENGTH_SHORT ).show();
    }

    @Override
    public void displayErrorConnection()
    {
        Toast.makeText( getContext(), "Error de conexión", Toast.LENGTH_SHORT ).show();
    }

    @Override
    public void displayAddMembersToGroup(GroupCreateResponse groupCreateResponse)
    {
        FragmentManager fm = getFragmentManager();
        ConfirmAddMembersDFragment addMembersDFragment = ConfirmAddMembersDFragment.newInstance( groupCreateResponse );
        addMembersDFragment.show( fm, "fragment_add_members_to_group" );
    }

    @Override
    public void setPresenter(CreateGroupContract.Presenter presenter)
    {
        mPresenter = presenter;
    }

    @Override
    public void closeCreateGroup()
    {
        dismiss();
    }
}
