package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.content.Context;

import com.unmsm.gps.app.danielneciosup.nikki.data.repository.GroupRepository;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.GroupsDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.local.GroupLocalDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.GroupRemoteDataSource;
import com.unmsm.gps.app.danielneciosup.nikki.util.RetrofitRemoteSingleton;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateRequest;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.GroupCreateResponse;
import com.unmsm.gps.app.danielneciosup.nikki.data.source.remote.service.GroupService;
import com.unmsm.gps.app.danielneciosup.nikki.util.SessionManager;

import org.json.JSONException;

import retrofit2.Retrofit;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public class CreateGroupPresenter implements CreateGroupContract.Presenter
{
    private CreateGroupContract.View mView;

    //private Retrofit retrofit;

    //private GroupService groupService;

    private SessionManager sessionManager;

    private GroupRepository groupRepository;

    public CreateGroupPresenter(CreateGroupContract.View mView, Context context)
    {
        this.mView = mView;
        //retrofit = RetrofitRemoteSingleton.getRetrofit();
        //groupService = retrofit.create( GroupService.class );
        sessionManager = new SessionManager( context );
        groupRepository = GroupRepository.getInstance(GroupLocalDataSource.getInstance(), GroupRemoteDataSource.getInstance());
    }

    @Override
    public void onCreateGroup( String nameGroup )
    {
        String userId = null;
        try
        {
            userId = sessionManager.getUserEntity().getUserid();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final GroupCreateRequest request = new GroupCreateRequest( nameGroup, userId );

        groupRepository.createGroup(new GroupsDataSource.LoadedGroupCreateCallback()
        {
            @Override
            public void onGroupCreateLoaded(GroupCreateResponse groupCreateResponse)
            {
                onCreateSuccessGroup( groupCreateResponse );
                onCloseCreateGroup();
                onAddMembersToGroup( groupCreateResponse );
            }

            @Override
            public void onErrorService()
            {
                mView.displayCreateError();
            }

            @Override
            public void onErrorConnection()
            {
                mView.displayErrorConnection();
            }
        }, request);
    }

    @Override
    public void onAddMembersToGroup(GroupCreateResponse groupCreateResponse)
    {
        mView.displayAddMembersToGroup( groupCreateResponse );
    }

    @Override
    public void onCreateSuccessGroup(GroupCreateResponse groupCreateResponse)
    {
        mView.displayCreateSuccess( groupCreateResponse );
    }

    @Override
    public void onCloseCreateGroup()
    {
        mView.closeCreateGroup();
    }
}
