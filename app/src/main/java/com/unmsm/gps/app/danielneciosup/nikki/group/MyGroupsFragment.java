package com.unmsm.gps.app.danielneciosup.nikki.group;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unmsm.gps.app.danielneciosup.nikki.R;

/**
 * Created by Daniel on 11/09/2016.
 */
public class MyGroupsFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState)
    {
        View myGroupsView = inflater.inflate(R.layout.fragment_my_groups, container, false);

        return myGroupsView;
    }
}
