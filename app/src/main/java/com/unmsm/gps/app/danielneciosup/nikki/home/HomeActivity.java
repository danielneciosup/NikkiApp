package com.unmsm.gps.app.danielneciosup.nikki.home;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.unmsm.gps.app.danielneciosup.nikki.R;
import com.unmsm.gps.app.danielneciosup.nikki.adapter.SectionsPagerAdapter;
import com.unmsm.gps.app.danielneciosup.nikki.group.MyGroupsFragment;
import com.unmsm.gps.app.danielneciosup.nikki.recents.RecentsFragment;
import com.unmsm.gps.app.danielneciosup.nikki.util.NikkiUtils;
import com.unmsm.gps.app.danielneciosup.nikki.util.ViewAppBarLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements HomeContract.View, ViewAppBarLayout, HomeContract.Presenter.OnHomeListener, HomeContract.OnFABClickListener
{
    @BindView( R.id.mdi_toolbar )
    Toolbar mdiToolbar;
    @BindView( R.id.mdi_view_pager )
    ViewPager mdiViewPager;
    @BindView( R.id.mdi_tab_layout )
    TabLayout mdiTabLayout;

    @BindView( R.id.drawer_layout )
    DrawerLayout drawerLayout;

    @BindView( R.id.fab_home )
    FloatingActionButton fabHome;

    private HomeContract.Presenter mPresenter;

    private static final int PAGER_DEFAULT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setPresenter( new HomePresenter( this, this ) );

        setupToolbar();
        setupViewPager( mdiViewPager );
        setupTabLayout();
        mPresenter.onSendAccessToken();
        mPresenter.setCurrentPage( PAGER_DEFAULT );
    }

    @Override
    public void setPresenter(HomeContract.Presenter presenter)
    {
        mPresenter = presenter;
    }

    @Override
    public void setupToolbar()
    {
        setSupportActionBar( mdiToolbar );
        final ActionBar actionBar = getSupportActionBar();
        if ( actionBar != null )
        {
            actionBar.setHomeAsUpIndicator( R.drawable.ic_reorder_white_24dp );
            actionBar.setDisplayHomeAsUpEnabled( true );
        }
    }

    @Override
    public void setupViewPager( ViewPager viewPager )
    {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter( getSupportFragmentManager(), this );
        adapter.addFragment( new RecentsFragment() );
        adapter.addFragment( new MyNotesFragment() );
        adapter.addFragment( new MyGroupsFragment() );
        mdiViewPager.setAdapter( adapter );
        mdiViewPager.setCurrentItem( PAGER_DEFAULT );
        mdiViewPager.setOnPageChangeListener( onPageHomeChangeListener() );
    }

    @Override
    public void setupTabLayout()
    {
        mdiTabLayout.setupWithViewPager( mdiViewPager );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if( !drawerLayout.isDrawerOpen( GravityCompat.START ) )
        {
            getMenuInflater().inflate( R.menu.nav_menu, menu );

            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch( item.getItemId() )
        {
            case android.R.id.home:
                drawerLayout.openDrawer( GravityCompat.START );
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showDialogAddNote()
    {
        CreateNoteDFragment dialogFragment = CreateNoteDFragment.newInstance();
        dialogFragment.setParentView(this.mdiViewPager);
        dialogFragment.show( getSupportFragmentManager(), "" );
    }

    @Override
    public void showDialogAddGroup()
    {
        mPresenter.onCreateGroup( getSupportFragmentManager() );
    }

    @Override
    public void setIconFABByNumberPage( int numberPage )
    {
        if( numberPage == NikkiUtils.PAGER_NOTES )
            fabHome.setImageResource( R.drawable.ic_note_add_white_24dp );
        else if ( numberPage == NikkiUtils.PAGER_GROUPS )
            fabHome.setImageResource( R.drawable.ic_group_add_white_24dp );
    }

    @Override
    public ViewPager.OnPageChangeListener onPageHomeChangeListener()
    {
        return new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled( int position, float positionOffset, int positionOffsetPixels )
            {

            }

            @Override
            public void onPageSelected( int position )
            {
                if ( position == NikkiUtils.PAGER_NOTES || position == NikkiUtils.PAGER_GROUPS )
                {
                    setIconFABByNumberPage( position );
                    fabHome.show();
                }
                else
                    fabHome.hide();

                mPresenter.setCurrentPage( position );
            }

            @Override
            public void onPageScrollStateChanged( int state )
            {

            }
        };
    }

    @Override
    @OnClick( R.id.fab_home )
    public void onFABClick()
    {
        if ( mPresenter.getCurrentPage() == NikkiUtils.PAGER_NOTES )
            showDialogAddNote();
        else if ( mPresenter.getCurrentPage() == NikkiUtils.PAGER_GROUPS )
            showDialogAddGroup();
    }
}
