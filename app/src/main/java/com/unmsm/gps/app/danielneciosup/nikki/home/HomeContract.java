package com.unmsm.gps.app.danielneciosup.nikki.home;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.unmsm.gps.app.danielneciosup.nikki.util.BaseView;

/**
 * Created by Daniel on 11/09/2016.
 */
public interface HomeContract
{
    interface OnFABClickListener
    {
        void onFABClick();
    }

    interface View extends BaseView<Presenter>
    {
        void showDialogAddNote();
        void showDialogAddGroup();
        void setIconFABByNumberPage( int numberPage );
    }

    interface Presenter
    {
        interface OnHomeListener
        {
            ViewPager.OnPageChangeListener onPageHomeChangeListener();
        }

        void onSendAccessToken();
        void setCurrentPage( int position );
        int getCurrentPage();
        void onCreateNote();
        void onCreateGroup(FragmentManager fragmentManager);
    }
}
