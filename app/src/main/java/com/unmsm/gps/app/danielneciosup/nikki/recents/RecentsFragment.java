package com.unmsm.gps.app.danielneciosup.nikki.recents;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unmsm.gps.app.danielneciosup.nikki.R;

/**
 * Created by Daniel on 11/09/2016.
 */
public class RecentsFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View myRecentsFragment = inflater.inflate(R.layout.fragment_recents, container, false);

        return myRecentsFragment;
    }
}
