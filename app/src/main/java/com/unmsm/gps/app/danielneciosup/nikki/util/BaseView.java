package com.unmsm.gps.app.danielneciosup.nikki.util;

/**
 * Created by Daniel on 10/09/2016.
 */
public interface BaseView<T>
{
    void setPresenter(T presenter);
}
