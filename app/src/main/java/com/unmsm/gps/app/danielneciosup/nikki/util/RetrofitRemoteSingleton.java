package com.unmsm.gps.app.danielneciosup.nikki.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DINJO-PC on 12/11/2016.
 */
public class RetrofitRemoteSingleton
{
    private static RetrofitRemoteSingleton instance;
    private static Retrofit retrofit;

    private RetrofitRemoteSingleton(){}

    public static Retrofit getRetrofit()
    {
        if ( instance == null && retrofit == null )
        {
            instance = new RetrofitRemoteSingleton();
            retrofit = new Retrofit.Builder()
                    .baseUrl( WS.URL_BASE )
                    .addConverterFactory( GsonConverterFactory.create() )
                    .build();
        }

        return retrofit;
    }
}