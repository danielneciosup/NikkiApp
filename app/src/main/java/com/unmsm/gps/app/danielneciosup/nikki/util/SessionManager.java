package com.unmsm.gps.app.danielneciosup.nikki.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.unmsm.gps.app.danielneciosup.nikki.data.bean.AccessTokenResponse;


import org.json.JSONException;

/**
 * Created by Yonkou Jean on 01/11/16.
 */
public class SessionManager {

    public static final String PREFERENCE_NAME = "ckeck_in";
    public static int PRIVATE_MODE = 0;

    /**
     USER DATA SESSION - JSON
     */
    public static final String ACCESS_TOKEN_RESPONSE = "user_token";
    public static final String USER_JSON = "user_json";
    public static final String IS_LOGIN = "user_login";
    public static final String ACCESS_TOKEN_RESPONS = "user_access";

    /**
     LAST LOCATION
     */
    public static final String LOCATION = "location";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREFERENCE_NAME,PRIVATE_MODE);
        editor = preferences.edit();
    }

    public boolean isLogin(){
        return preferences.getBoolean(IS_LOGIN,false);
    }

    public void openSession(String token, AccessTokenResponse userEntity){
        editor.putBoolean(IS_LOGIN,true);
        editor.putString( ACCESS_TOKEN_RESPONSE,token);
        if(userEntity != null){
            Gson gson = new Gson();
            String user = gson.toJson(userEntity);
            editor.putString(ACCESS_TOKEN_RESPONS,user);
        }
        editor.commit();
    }

    public void setAccessTokenResponse( AccessTokenResponse accessTokenResponse){
        if(accessTokenResponse!=null){
            editor.putBoolean(IS_LOGIN,true);

            Gson gson = new Gson();
            String user = gson.toJson(accessTokenResponse);
            editor.putString(ACCESS_TOKEN_RESPONS,user);
        }
        editor.commit();
    }

    public void setSomeEntity(@NonNull String key,@NonNull Object entity,@NonNull Class c ){
        if( entity != null ){
            Gson gson = new Gson();
            String entitySerialized = gson.toJson(c.cast(entity));
            editor.putString(key,entitySerialized);
        }
        editor.commit();
    }

    public Object getSomeEntity(@NonNull String key,@NonNull Class c ) throws JSONException{
        String entitySerialized = preferences.getString(key, null);
        Object o = new Gson().fromJson(entitySerialized, c);
        return o;
    }

    public void deleteSomeEntity(@NonNull String key){
        editor.remove(key);editor.commit();
    }

    public void closeSession(){
        editor.putBoolean(IS_LOGIN,false);
        editor.putString(ACCESS_TOKEN_RESPONSE,null);
        editor.putString(USER_JSON,null);

        editor.commit();
    }

    public AccessTokenResponse getUserEntity() throws JSONException{
        if(isLogin()){
            String userData = preferences.getString(ACCESS_TOKEN_RESPONS,null);
            AccessTokenResponse userEntity = new Gson().fromJson(userData,AccessTokenResponse.class);
            return  userEntity;
        }else{
            return null;
        }
    }

    public String getUserToken(){
        if(isLogin()){
            return preferences.getString(ACCESS_TOKEN_RESPONSE,"");
        }else{
            return null;
        }
    }

}