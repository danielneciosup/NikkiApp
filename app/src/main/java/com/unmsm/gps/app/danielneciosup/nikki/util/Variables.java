package com.unmsm.gps.app.danielneciosup.nikki.util;

import com.unmsm.gps.app.danielneciosup.nikki.R;

/**
 * Created by Daniel on 02/10/2016.
 */
public class Variables
{
    public static final int[] ARRAY_ICONS_TAB_LAYOUT = { R.drawable.ic_query_builder_white_24dp,
                                                         R.drawable.ic_insert_drive_file_white_24dp,
                                                         R.drawable.ic_group_white_24dp };
    public static final int PAGER_NOTES = 1;
    public static final int PAGER_GROUPS = 2;
}
